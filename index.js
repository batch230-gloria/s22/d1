// console.log ("Hello world!");

// Javascript has built-in functions and methods for arrays. This allows us to manipulate and acces array items
console.log("=============== Mutator =================");
// Mutator methos
	/*
		- Mutator methods are functions that "mutatate" or change an array after they're created
		- The mutator methods manipulate the original array performing various taks such as adding and removing elements.
	*/

	let fruits = ['Apple', 'Orange', 'Chico', 'Lemon'];
	console.log("---------- push ------------");
	// push()

	console.log('Current array: ');
	console.log(fruits);
	
	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength);
	console.log(fruits);


	// without return;
	/*
	function addFruit(newFruit){
		fruits.push(newFruit)
		console.log(fruits);		
	}
	addFruit('Gum gum fruit');
	*/
	
	/*
	function addFruit(fruit){
		let newFruitArray = ['Apple', 'Orange', 'Chico', 'Lemon'];
		fruits.push(fruit)
		console.log(newFruitArray);		
	}
	console.log("Output from newFruitArray");
	addFruit('Gum gum fruit');

	console.log("Output from fruits array");
	console.log(fruits);
	*/
	

	// with return;
	/*
	function addFruit(newFruit){
		fruits.push(newFruit)
		console.log(fruits);
		return newFruit;
	}

	let addedFruit = addFruit("Gum gum fruit");
	console.log(newFruit);
	*/

	console.log("---------- pop ------------");
	// pop()
	/*
		Removes that last element in an array AND returns the removes element
		-Syntax
			arrayName.pop();
	*/
	// fruits = ['Apple', 'Orange', 'Chico', 'Lemon', 'Mango'];
	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from pop method: ");
	console.log(fruits);


	console.log("---------- unshift ------------");
	// unshift
	/*
		- Adds one or more elements at the beginning of an array
		- Syntax
			arrayName.unshift('elementA');
			arrayName.unshift('elementA','elementB');
	*/
	fruits.unshift('Lime', 'Banana');
	console.log("Mutated array from unshift method: ");
	console.log(fruits);

	console.log("---------- shift ------------");
	// shift
	/*
		- Removes an element at the beginning of an array AND returns the removes element;
		- Syntax
			arrayName.shift();
	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ")
	console.log(fruits);


	console.log("---------- splice ------------");
	// splice
	/*
		- Simultaneously removes elements from a specified index number and adds element/elements
		- Syntax
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
	*/

	fruits.splice(2,3,'Coconut','Tomatoe','Atis');
	console.log("Mutated array from splice method: ");
	console.log(fruits);

	// Change elements apple and coconut to lime and cherry
	fruits.splice(1,2,'Lime','Cherry');
	console.log("Mutated array from splice method: ");
	console.log(fruits);


	console.log("---------- sort ------------");
	// sort()
	/*
		- Rearranges the array elements in alphanumeric order 
		- Syntax 
			arrayName.sort();
	*/

	fruits.sort();
	console.log("Mutated array from sort method: ");
	console.log(fruits);


	console.log("---------- reverse ------------");
	// reverse()
	/*
		- Reverses the order of array elements
		- Syntax
			arrayName.reverse();
	*/
	fruits.reverse();
	console.log("Mutated array from reverse method: ");
	console.log(fruits);


	console.log("=============== Non-Mutator =================");
	// Non-Mutator Methods
	/*	
		- Non-Mutator methods are functions that do not modify or change and array after they're created
		- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing theo output
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE',];
	console.log("---------- indexOf ------------");
	// indexOf()
	/*	
		- Returns the index number of the first matching element
		- If no match was found, the result will be -1;
		-The search process will be done from first element proceeding to the last element
		-Syntax
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, fromIndex);
	*/

	let firstIndex = countries.indexOf('PH');
	console.log("Result of indexOf('PH'): " + firstIndex);

	let secondIndex = countries.indexOf('PH', 2);
	console.log("Result of indexOf('PH'): " + secondIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log("Result of indexOf('BR'): " + invalidCountry);


	console.log("---------- lastIndex ------------");
	// lastIndexOf()
	/*
		- Returns the index number of the last matching element found in an array;
		- The search process will be done from last element proceeding to the first element
		-Syntax
			arrayName.lastNameOf(searchValue);
			arrayName.lastNameOf(searchValue, fromIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log("Result of lastIndexOf() method: " + lastIndex);

	let lastIndexStart = countries.lastIndexOf('PH', 4);
	console.log("Result of lastIndexOf() method: " + lastIndexStart);	


	console.log("---------- toString ------------");
	// toString()
	let stringArray = countries.toString();
	console.log("Result from toString() method: ");
	console.log(stringArray);


	console.log("---------- concat ------------");
	// concat()
	/*
		- Combines two arrays and returns the combined result 
		- Syntax
			arrayA.concat(arrayB);
			arrayA.concat(elementA);
	*/

	let taskArrayA = ['drink html', 'eat javascript'];
	let taskArrayB = ['inhale css', 'breathe bootstrap'];
	let taskArrayC = ['get git', 'cook node'];

	// Combining 2 arrays
	let tasks = taskArrayA.concat(taskArrayB);
	console.log(tasks);

	// Combining MultipleArray
	let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTasks);

	// Combining arrays with elements
	let combinedTasks = taskArrayA.concat('smell express', 'have react');
	console.log(combinedTasks);


	console.log("---------- join ------------");
	// join()
	/*
		- Returns an array as a string seperated by specified separator string
		- Syntax
			arrayName.join('seperator');
	*/

	let users = ['John', 'Jane', 'Joe', 'Robert'];
	console.log(users.join(' - '));




	console.log("=============== Iteration Methods =================");
	// Iteration Methods

	console.log("---------- forEach ------------");
	// forEach()

	// AllTask
	// ['drink html', 'eat javascript', 'inhale css', 'breathe bootstrap', 'get git', 'cook node']

	allTasks.forEach(function(perElement){
		console.log(perElement);
	});


	/*
	let filteredTasks = [];
	allTasks.forEach(function(perElement){
		if(perElement.length > 10){
			filteredTasks.push(perElement);
		}
	});

	console.log("Result of filteredTasks: ");
	console.log(filteredTasks);
*/


	console.log("---------- map ------------");
	//  map()
	/*
		- Iterates on each element AND returns new array with different values depending on the result of the function's operation
        - This is useful for performing tasks where mutating/changing the elements are required
        - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation

		- Syntax
		let/const resultArray = arrayName.map(function(indivElement))
	*/
	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(element){
		return element * 10;
	})

	console.log(numbers);
	console.log(numberMap);

	// map() vs forEach()

	let numberForEach = numbers.forEach(function(element){
		return element * 10;
	})

	 console.log(numberForEach);
	 //forEach(), loops over all items in the array as does map(), but forEach() does not return a new array.


	 console.log("---------- every ------------");
	 // every()
	 /*
        - Checks if all elements in an array meet the given condition
        - This is useful for validating data stored in arrays especially when dealing with large amounts of data
        - Returns a true value if all elements meet the condition and false if otherwise
        - Syntax
            let/const resultArray = arrayName.every(function(indivElement) {
                return expression/condition;
            })
    */
 	// let numbers = [1, 2, 3, 4, 5];
	let allValid = numbers.every(function(element) {
        return (element < 3);
    });

    console.log("Result of every method: ");
    console.log(allValid);



	console.log("---------- some ------------");
    // some()

    let someValid = numbers.some(function(element){
    	 return (element < 2);
    });

    console.log("Result of some method: ");
    console.log(someValid);


	console.log("---------- filter ------------");
    // filter()
    /*
        - Returns new array that contains elements which meets the given condition
        - Returns an empty array if no elements were found
        - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
        - Mastery of loops can help us work effectively by reducing the amount of code we use
        - Several array iteration methods may be used to perform the same result
        - Syntax
            let/const resultArray = arrayName.filter(function(indivElement) {
                return expression/condition;
            })
    */
    let filterValid = numbers.filter(function(element){
    	return (element < 3);
    });
    console.log("Result of filter method: ");
    console.log(filterValid);

    // No elements Found
    let nothingFound = numbers.filter(function(element){
    	return(element == 0);
    })
    console.log(nothingFound);
    


	console.log("---------- includes ------------");
    // includes()
    /*
    	- includes() method checks if the argument passed can be found in the array.
        - it returns a boolean which can be saved in a variable.
            - returns true if the argument is found in the array.
            - returns false if it is not.
        - Syntax:
            arrayName.includes(<argumentToFind>)
     */

    let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

    let productsFound1 = products.includes("Mouse");
    console.log(productsFound1);

    let productsFound2 = products.includes("Headset");
    console.log(productsFound2);



    console.log("---------- reduce ------------");
    // reduce() 
    /* 
        - Evaluates elements from left to right and returns/reduces the array into a single value
        - Syntax
            let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
                return expression/operation
            })
        - The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
        - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
    */

    // let numbers = [1, 2, 3, 4, 5];
    let i = 0;
    let reducedArray = numbers.reduce(function(acc,cur){
    	console.warn('current iteration'+ ++i);
    	console.log('accumulator: '+ acc) // 1 // 3 // 6 // 10 // 15
    	console.log('current value: '+ cur) // 2 // 3 // 4 // 5

    	return acc + cur; // 3 // 6 // 10 // 15
    })

    console.log("Result of reduce method: ");
    console.log(reducedArray);


